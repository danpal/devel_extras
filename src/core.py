#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Nov 28 18:36:25 2020

Copyright (c) 2020 Daniel Esteban Palma Igor <daniel.palma.i@ug.uchile.cl>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

.. codeauthor:: Daniel Esteban Palma Igor <daniel.palma.i@ug.uchile.cl>
"""

from collections import UserDict
import hashlib
import importlib
from importlib.abc import MetaPathFinder
import os
import pickle
from pkg_resources import find_distributions
import sys
import zipfile
from types import ModuleType, MethodType
from typing import Union, Tuple, List, Optional

import appdirs
from chimerax.bundle_builder import BundleBuilder
from chimerax.core.commands import (
    CmdDesc,
    StringArg,
    OpenFileNameArg,
    OpenFolderNameArg,
    BoolArg,
    AnnotationError,
)
from chimerax.core.errors import UserError
from chimerax.core.session import Session
from chimerax.core.toolshed import BundleInfo, CommandInfo, ToolInfo, Toolshed
from chimerax.core.toolshed.installed import _make_bundle_info
from chimerax.core.triggerset import TriggerSet


class ModuleArg(StringArg):
    """Annotation for a python module name"""

    name = "a python module name"

    @staticmethod
    def parse(text: str, session: Session):
        token, text, rest = StringArg.parse(text, session)
        for var in token.split("."):
            if not var.isidentifier():
                raise AnnotationError("Invalid module name")
        return token, text, rest


def _create_triggers():
    ts = TriggerSet()
    ts.add_trigger("new bundle")
    ts.add_trigger("select bundle")
    ts.add_trigger("reload module")
    return ts


TRIGGERS = _create_triggers()


class PathDict(UserDict):
    def __getitem__(self, key):
        if isinstance(key, str):
            key = os.path.abspath(key)
        if key in self.data:
            return self.data[key]
        if hasattr(self.__class__, "__missing__"):
            return self.__class__.__missing__(self, key)
        raise KeyError(key)

    def __setitem__(self, key, item):
        if isinstance(key, str):
            key = os.path.abspath(key)
            self.data[key] = item
        else:
            raise TypeError(f"type {type(key)} instead of str")

    def __delitem__(self, key):
        if isinstance(key, str):
            key = os.path.abspath(key)
        del self.data[key]

    def __contains__(self, key):
        if isinstance(key, str):
            key = os.path.abspath(key)
        return key in self.data


class BundlePathDict(PathDict):
    def __setitem__(self, key, item):
        if isinstance(key, str):
            key = os.path.abspath(key)
            self.data[key] = item
            TRIGGERS.activate_trigger("new bundle", None)
        else:
            raise TypeError(f"type {type(key)} instead of str")


def _read_pickle(path):
    with open(path, "rb") as file:
        return pickle.load(file)


def _write_pickle(obj, path):
    with open(path, "wb") as file:
        pickle.dump(obj, file)


def _assert_type(obj, assert_type):
    obj_type = type(obj)
    if obj_type != assert_type:
        raise TypeError(f"Incorrect type {obj_type}, it must be {assert_type}")


def _get_path_dict(filename: str):
    data_dir = appdirs.user_data_dir("ChimeraX-DevelExtras", "DevelExtras")
    os.makedirs(data_dir, exist_ok=True)
    path = os.path.join(data_dir, filename)
    if not os.path.isfile(path):
        return PathDict()
    else:
        bmd5 = _read_pickle(path)
        _assert_type(bmd5, PathDict)
        return bmd5


def _write_path_dict(path_dict, filename: str):
    _assert_type(path_dict, PathDict)
    data_dir = appdirs.user_data_dir("ChimeraX-DevelExtras", "DevelExtras")
    os.makedirs(data_dir, exist_ok=True)
    path = os.path.join(data_dir, filename)
    _write_pickle(path_dict, path)


def _get_bundles_md5():
    return _get_path_dict("bundles_md5.pkl")


def _write_bundles_md5(session):
    return _write_path_dict(session.devel.bundles_md5, "bundles_md5.pkl")


def _get_bundles_on_start():
    return _get_path_dict("bundles_on_start.pkl")


def _write_bundles_on_start(session):
    return _write_path_dict(
        session.devel.bundles_on_start, "bundles_on_start.pkl"
    )


BUNDLES = BundlePathDict()


class DevelExtrasSession:
    def __init__(self):
        self.bundles = BUNDLES
        self.bundles_md5 = _get_bundles_md5()
        self.bundles_on_start = _get_bundles_on_start()


class ImportedFromPathFinder(MetaPathFinder):
    imported = {}

    @classmethod
    def find_spec(cls, fullname, path, target=None):
        return cls.imported.get(fullname)

    @classmethod
    def invalidate_caches(cls):
        """An optional method for clearing the finder's cache, if any.
        This method is used by importlib.invalidate_caches().
        """


if ImportedFromPathFinder not in sys.meta_path:
    sys.meta_path.append(ImportedFromPathFinder)

mr_desc = CmdDesc(
    required=[("module", ModuleArg)],
    keyword=[("submodules", BoolArg), ("warnings_as_errors", BoolArg)],
    synopsis="Reloads the specified Python module and its submodules",
)


def module_reload(
    session: Session,
    module: Union[ModuleType, str],
    *,
    submodules: bool = True,
    warnings_as_errors: bool = False,
) -> List[ModuleType]:
    """
    Reloads the specified module and its submodules.

    Parameters
    ----------
    session : Session
        ChimeraX session.
    module : Union[ModuleType, str]
        Module instance or its name as an absolute import path.
    reload_submodules : bool, optional
        If reload the submodules. The default is True.
    warnings_as_errors : bool, optional
        If raise `ImportError` instead of print warnings. The default is False.

    Raises
    ------
    TypeError
        If `module` is of the wrong type.
    ImportError
        If `warnings_as_errors` and could NOT reload the module.
    UserError
        If could NOT find the module.

    Returns
    -------
    List[ModuleType]
        List of modules found (reloaded or not).

    """

    info = session.logger.info
    warn = session.logger.warning
    if isinstance(module, str):
        name = module
    elif isinstance(module, ModuleType):
        name = module.__name__
    else:
        raise TypeError("Must specify a module or its name")
    split_name = name.split(".")
    length = len(split_name)
    modules = []
    for n, module in sys.modules.items():
        if submodules:
            split_n = n.split(".")
            if split_n[:length] == split_name:
                modules.append(module)
        else:
            if n == name:
                modules.append(module)
                break
    if modules:
        TRIGGERS.activate_trigger("reload module", None)
        modules.sort(key=lambda x: x.__name__, reverse=True)
        yes = 0
        no = 0
        for module in modules:
            try:
                importlib.reload(module)
                info(f"<b>Reloaded:</b> {module.__name__}", is_html=True)
                yes += 1
            except Exception as err:
                text = f"Could NOT reload the module {module.__name__}"
                if hasattr(module, "__file__"):
                    text += f" ({module.__file__})"
                if warnings_as_errors:
                    raise ImportError(text) from err
                else:
                    warn(f"{text}: {err}")
                    no += 1
        info(f"<b>Reloaded {yes} modules</b>", is_html=True)
        if no:
            warn(f"Could NOT reload {no} modules")
    else:
        raise UserError(
            f"Could NOT find the module {module} "
            "(remember to use absolute import paths)"
        )
    return modules


def _init_managers(session, bundle_info):
    ts = session.toolshed
    for mgr, kw in bundle_info.managers.items():
        if not session.ui.is_gui and kw.pop("guiOnly", False):
            continue
        all_bundles = list(session.devel.bundles.values())
        all_bundles += ts._installed_bundle_info
        if ts._available_bundle_info:
            all_bundles += ts._available_bundle_info

        m = bundle_info.init_manager(session, mgr, **kw)
        ts._manager_instances[mgr] = m
        for p_bundle_info in all_bundles:
            for name, kw in p_bundle_info.providers.items():
                p_mgr, pvdr = name.split("/", 1)
                if p_mgr == mgr:
                    m.add_provider(p_bundle_info, pvdr, **kw)
        m.end_providers()


def _add_providers(session, bundle_info):
    ts = session.toolshed
    for name, kw in bundle_info.providers.items():
        mgr_name, pvdr_name = name.split("/", 1)
        mgr = ts._manager_instances.get(mgr_name, None)
        if mgr:
            mgr.add_provider(bundle_info, pvdr_name, **kw)


def _register_commands(bundle_info: BundleInfo, logger):
    for ci in bundle_info.commands:
        bundle_info._register_cmd(ci, logger)


def _register_bundle_info(
    session: Session, bundle_info: BundleInfo, delayed: bool = True
):
    _init_managers(session, bundle_info)
    _add_providers(session, bundle_info)
    if delayed:
        bundle_info.register(session.logger)
    else:
        _register_commands(bundle_info, session.logger)
        bundle_info._register_selectors(session.logger)


rmc_desc = CmdDesc(
    required=[("module", ModuleArg)],
    keyword=[("delayed", BoolArg)],
    synopsis="Registers a module's bundle",
)


def register_module(session: Session, module: str, *, delayed: bool = True):
    info = session.logger.info
    module = module.strip()
    for bundle_info in session.devel.bundles.values():
        package = bundle_info.package_name
        if module == package:
            _register_bundle_info(session, bundle_info, delayed)
            info(f"<b>Registered bundle of:</b> {module}", is_html=True)
            return
    raise UserError(f"NO bundle for module {module}")


ifp_desc = CmdDesc(
    required=[("module_name", ModuleArg), ("file_path", OpenFileNameArg)],
    synopsis="Imports Python module from file path",
)


def import_from_path(
    session: Session, module_name: str, file_path: str
) -> ModuleType:
    """
    Imports Python module from file path.

    Parameters
    ----------
    session : Session
        ChimeraX session.
    module_name : str
        Module name.
    file_path : str
        File path.

    Returns
    -------
    module : ModuleType
        Imported module.

    """
    info = session.logger.info
    spec = importlib.util.spec_from_file_location(module_name, file_path)
    ImportedFromPathFinder.imported[module_name] = spec
    module = importlib.util.module_from_spec(spec)
    sys.modules[module_name] = module
    spec.loader.exec_module(module)
    info(
        f"<b>module {module_name} imported from:</b> {file_path}", is_html=True
    )
    return module


rbc_desc = CmdDesc(
    required=[("bundle_path", OpenFolderNameArg)],
    keyword=[("delayed", BoolArg)],
    synopsis="Registers a loaded bundle",
)


def register_bundle_info(
    session: Session, bundle_path: str, *, delayed: bool = True
):
    info = session.logger.info
    if bundle_path in session.devel.bundles:
        bundle_info = session.devel.bundles[bundle_path]
        _register_bundle_info(session, bundle_info, delayed)
        info(f"<b>Registered bundle from:</b> {bundle_path}", is_html=True)
        return
    raise UserError(f"NOT a loaded bundle: {bundle_path}")


Info = List[Tuple[str, List[str], str]]


def _parse_classifiers(classifiers: List[str], commands: Info, tools: Info):
    for classifier in classifiers:
        fields = [i.strip() for i in classifier.split("::")]
        if len(fields) == 5:
            classifier_type = fields[1].lower()
            name = fields[2]
            categories = fields[3].split(",")
            synopsis = fields[4]
            if classifier_type == "command":
                commands.append((name, categories, synopsis))
            elif classifier_type == "tool":
                tools.append((name, categories, synopsis))


def _add_tools(bundle_info: BundleInfo, tools: Info):
    for tool in tools:
        tool_info = ToolInfo(*tool)
        bundle_info.tools.append(tool_info)


def _add_commands(bundle_info: BundleInfo, commands: Info):
    for command in commands:
        command_info = CommandInfo(*command)
        bundle_info.commands.append(command_info)


def get_md5(path):
    with open(path, "rb") as file:
        return hashlib.md5(file.read()).digest()


def has_bundle_info_changed(session, bundle_path, md5):
    if session.devel.bundles_md5.get(bundle_path) == md5:
        return False
    return True


def _has_dist(bundle_path):
    dist_path = os.path.join(bundle_path, "dist")
    if os.path.isdir(dist_path):
        for path in os.listdir(dist_path):
            if path.endswith(".dist-info"):
                return True
    return False


def _make_bundle(session, bundle_path):
    dist_path = os.path.join(bundle_path, "dist")
    bbuilder = BundleBuilder(session.logger, bundle_path)
    bbuilder.make_clean()
    bbuilder.make_wheel()
    with zipfile.ZipFile(bbuilder.wheel_path) as file:
        for name in file.namelist():
            dir_path = os.path.split(name)[0]
            if dir_path.endswith(".dist-info"):
                file.extract(name, dist_path)


def _load_bundle(session, bundle_path):
    info = session.logger.info
    dist_path = os.path.join(bundle_path, "dist")
    dist = list(find_distributions(dist_path))[0]
    bundle_info = _make_bundle_info(dist, True, session.logger)
    session.devel.bundles[bundle_path] = bundle_info
    info(
        f"<b>{bundle_info.name} BundleInfo loaded from:</b> {bundle_path}",
        is_html=True,
    )
    return bundle_info


lbi_desc = CmdDesc(
    required=[("bundle_path", OpenFolderNameArg)],
    synopsis="Loads a BundleInfo object from a folder path",
)


def load_bundle_info(
    session: Session, bundle_path: str
) -> Tuple[str, BundleInfo]:
    info = session.logger.info
    bundle_path = os.path.abspath(bundle_path)
    xml_path = os.path.join(bundle_path, "bundle_info.xml")
    md5 = get_md5(xml_path)
    if has_bundle_info_changed(session, bundle_path, md5):
        _make_bundle(session, bundle_path)
        bundle_info = _load_bundle(session, bundle_path)
        session.devel.bundles_md5[bundle_path] = md5
        _write_bundles_md5(session)
    elif bundle_path in session.devel.bundles:
        bundle_info = session.devel.bundles[bundle_path]
        info(
            f"<b>{bundle_info.name} Bundle Info has NOT changed:</b> {bundle_path}",
            is_html=True,
        )
    elif _has_dist(bundle_path):
        bundle_info = _load_bundle(session, bundle_path)
    else:
        _make_bundle(session, bundle_path)
        bundle_info = _load_bundle(session, bundle_path)
    return bundle_path, bundle_info


lb_desc = CmdDesc(
    required=[("bundle_path", OpenFolderNameArg)],
    keyword=[
        ("reload_module", BoolArg),
        ("initialization", BoolArg),
        ("register", BoolArg),
        ("load_on_start", BoolArg),
    ],
    synopsis="Loads a Bundle from a folder path",
)


def load_bundle(
    session: Session,
    bundle_path: str,
    *,
    reload_module: bool = True,
    initialization: bool = True,
    register: bool = True,
    load_on_start: Optional[bool] = None,
) -> Tuple[str, BundleInfo]:
    bundle_path, bundle_info = load_bundle_info(session, bundle_path)
    package = bundle_info.package_name
    if package in sys.modules and reload_module:
        module_reload(session, package)
    else:
        import_from_path(
            session, package, os.path.join(bundle_path, "src", "__init__.py")
        )
    if initialization:
        if bundle_info.custom_init:
            bundle_info.initialize(session)
    if register:
        register_bundle_info(session, bundle_path)
    if load_on_start is not None:
        load_bundle_on_start(session, load_on_start, bundle_path)
    return bundle_path, bundle_info


lbos_desc = CmdDesc(
    required=[("load_on_start", BoolArg), ("bundle_path", OpenFolderNameArg)],
    synopsis="Controls whether a bundle is loaded at the ChimeraX start",
)


def load_bundle_on_start(session, load_on_start: bool, bundle_path: str):
    session.devel.bundles_on_start[bundle_path] = load_on_start
    _write_bundles_on_start(session)


def find_bundle(self, name, logger, installed=True, version=None):
    lc_name = name.casefold().replace("_", "-")
    lc_names = [lc_name]
    if not lc_name.startswith("chimerax-"):
        lc_names.append("chimerax-" + lc_name)
    for bi in BUNDLES.values():
        if bi.name.casefold() in lc_names:
            return bi
    return Toolshed.find_bundle(
        self, name, logger, installed=installed, version=version
    )


def find_bundle_for_tool(self, name, prefix_okay=False):
    lc_name = name.casefold()
    tools = []
    for bi in BUNDLES.values():
        for tool in bi.tools:
            tname = tool.name.casefold()
            if tname == lc_name or (prefix_okay and tname.startswith(lc_name)):
                tools.append((bi, tool.name))
    if tools:
        return tools
    return Toolshed.find_bundle_for_tool(self, name, prefix_okay=prefix_okay)


def find_bundle_for_command(self, cmd):
    for bi in BUNDLES.values():
        for ci in bi.commands:
            if ci.name == cmd:
                return bi
    return Toolshed.find_bundle_for_command(self, cmd)


def find_bundle_for_class(self, cls):
    packages = {}
    for bi in BUNDLES.values():
        packages[tuple(bi.package_name.split("."))] = bi
    package = tuple(cls.__module__.split("."))
    while package:
        try:
            return packages[package]
        except KeyError:
            pass
        package = package[0:-1]
    return Toolshed.find_bundle_for_class(self, cls)


def _replace_toolshed_methods(toolshed):
    toolshed.find_bundle = MethodType(find_bundle, toolshed)
    toolshed.find_bundle_for_tool = MethodType(find_bundle_for_tool, toolshed)
    toolshed.find_bundle_for_command = MethodType(
        find_bundle_for_command, toolshed
    )
    toolshed.find_bundle_for_class = MethodType(
        find_bundle_for_class, toolshed
    )


def _restore_toolshed_methods(toolshed):
    toolshed.find_bundle = MethodType(Toolshed.find_bundle, toolshed)
    toolshed.find_bundle_for_tool = MethodType(
        Toolshed.find_bundle_for_tool, toolshed
    )
    toolshed.find_bundle_for_command = MethodType(
        Toolshed.find_bundle_for_command, toolshed
    )
    toolshed.find_bundle_for_class = MethodType(
        Toolshed.find_bundle_for_class, toolshed
    )
