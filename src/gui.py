#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 27 20:53:54 2020

Copyright (C) 2020  Daniel Esteban Palma Igor <daniel.palma.i@ug.uchile.cl>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

.. codeauthor:: Daniel Esteban Palma Igor <daniel.palma.i@ug.uchile.cl>
"""
from typing import Iterable

from chimerax.core.commands import run
from chimerax.core.settings import Settings
from chimerax.core.tools import ToolInstance
from chimerax.ui import MainToolWindow
from chimerax.ui.options import (
    InputFolderOption,
    Option,
    SettingsPanel,
    OptionsPanel,
    BooleanOption,
)
from chimerax.ui.widgets import ItemMenuButton
from Qt.QtWidgets import (
    QDialogButtonBox as qbbox,
    QHBoxLayout,
    QLabel,
    QPushButton,
    QVBoxLayout,
    QWidget,
)

from .core import TRIGGERS


class ItemMenuOption(Option):
    def set_multiple(self):
        pass

    def get_value(self):
        return self.widget.value

    def set_value(self, val):
        self.widget.value = val

    value = property(get_value, set_value)

    def _make_widget(self, **kw):
        self.widget = ItemMenuButton(**kw)
        self.widget.value_changed.connect(self.make_callback)


class Opt:
    def __init__(
        self,
        attr_name: str,
        option: Option,
        name: str,
        default=None,
        callback=lambda x: None,
        **kwargs,
    ):
        self.attr_name = attr_name
        self.option = option
        self.name = name
        self.default = default
        self.callback = callback
        self.kwargs = kwargs


def set_options(
    panel: SettingsPanel,
    settings: Settings,
    options: Iterable[Opt],
    category=None,
):
    for opt in options:
        option = opt.option(
            opt.name,
            opt.default,
            opt.callback,
            attr_name=opt.attr_name,
            settings=settings,
            **opt.kwargs,
        )
        setattr(panel, opt.attr_name, option)
        if category is not None:
            panel.add_option(category, option)
        else:
            panel.add_option(option)


class DevelExtrasSettings(Settings):
    EXPLICIT_SAVE = {
        "bundle_path": None,
        "reload_module": True,
        "initialization": True,
        "register": True,
        "load_on_start": False,
    }


class DevelExtrasSettingsPanel(SettingsPanel):
    def __init__(self, session, settings, **kwargs):
        super().__init__(**kwargs)
        self.session = session
        options = [
            Opt(
                "bundle_path",
                InputFolderOption,
                "Bundle path",
                callback=self._on_select,
            ),
            Opt("reload_module", BooleanOption, "Reload module"),
            Opt("initialization", BooleanOption, "Initialize"),
            Opt("register", BooleanOption, "Register"),
            Opt("load_on_start", BooleanOption, "Load on start"),
        ]

        set_options(self, settings, options)

    def _on_select(self, option):
        bundle_path = self.bundle_path.value
        load_on_start = self.session.devel.bundles_on_start.get(bundle_path)
        if load_on_start is not None:
            self.load_on_start.value = load_on_start


def _get_bundle_tools(bundle_info):
    if bundle_info:
        return bundle_info.tools
    else:
        return []


class DevelExtrasOptionsPanel(OptionsPanel):
    def __init__(self, session, **kwargs):
        super().__init__(**kwargs)
        self.bundle_info = ItemMenuOption(
            "Loaded Bundles",
            None,
            self._on_select_bundle,
            list_func=session.devel.bundles.values,
            key_func=lambda x: x.name,
            item_text_func=lambda x: x.name,
            trigger_info=[(TRIGGERS, "new bundle")],
        )
        self.tool = ItemMenuOption(
            "Bundle Tools",
            None,
            lambda x: None,
            list_func=lambda: _get_bundle_tools(self.bundle_info.value),
            key_func=lambda x: x.name,
            item_text_func=lambda x: x.name,
            trigger_info=[(TRIGGERS, "select bundle")],
        )
        self.add_option(self.bundle_info)
        self.add_option(self.tool)

    def _on_select_bundle(self, option):
        TRIGGERS.activate_trigger("select bundle", None)


class SettingsButtons(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.label = QLabel("Bundle")
        self.load = QPushButton("Load")

        layout = QHBoxLayout()
        layout.addWidget(self.label)
        layout.addWidget(self.load)
        self.setLayout(layout)


class OptionsButtons(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.label = QLabel("Tool")
        self.start = QPushButton("Start")

        layout = QHBoxLayout()
        layout.addWidget(self.label)
        layout.addWidget(self.start)
        self.setLayout(layout)


def run_command(session, command: str, *args, **kwargs):
    for arg in args:
        if arg or arg == 0 or type(arg) == bool:
            command += f" {arg}"
    for key, arg in kwargs.items():
        if arg or arg == 0 or type(arg) == bool:
            command += f" {key} {arg}"
    return run(session, command)


class DevelExtrasTool(ToolInstance):
    """A tool to open non installed Bundle Tools"""

    def __init__(self, session, tool_name: str):
        super().__init__(session, tool_name)

        self.tool_window = MainToolWindow(self)
        self.parent = self.tool_window.ui_area
        self.settings = DevelExtrasSettings(
            self.session, self.__class__.__name__
        )

        self.spanel = DevelExtrasSettingsPanel(
            self.session, self.settings, sorting=False
        )
        self.sbuttons = SettingsButtons()
        self.sbuttons.load.clicked.connect(self.load_bundle)

        self.opanel = DevelExtrasOptionsPanel(self.session, sorting=False)
        self.obuttons = OptionsButtons()
        self.obuttons.start.clicked.connect(self.start_tool)

        self.bbox = qbbox(qbbox.Close | qbbox.Help)
        self.bbox.rejected.connect(self.delete)

        self.layout = QVBoxLayout()
        self.layout.addWidget(self.spanel)
        self.layout.addWidget(self.sbuttons)
        self.layout.addWidget(self.opanel)
        self.layout.addWidget(self.obuttons)
        self.layout.addWidget(self.bbox)
        self.parent.setLayout(self.layout)

        self.tool_window.manage(placement=None)

    def load_bundle(self):
        bundle_path = self.spanel.bundle_path.value
        reload_module = self.spanel.reload_module.value
        initialization = self.spanel.initialization.value
        register = self.spanel.register.value
        load_on_start = self.spanel.load_on_start.value
        run_command(
            self.session,
            "devel bundle",
            bundle_path,
            reloadModule=reload_module,
            initialization=initialization,
            register=register,
            loadOnStart=load_on_start,
        )

    def start_tool(self):
        bundle_info = self.opanel.bundle_info.value
        tool = self.opanel.tool.value
        bundle_info.start_tool(self.session, tool.name)
