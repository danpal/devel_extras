#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 13 13:04:28 2020

Copyright (C) 2020  Daniel Esteban Palma Igor <daniel.palma.i@ug.uchile.cl>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

.. codeauthor:: Daniel Esteban Palma Igor <daniel.palma.i@ug.uchile.cl>
"""

from chimerax.core.toolshed import BundleAPI
from chimerax.core.commands import register
from . import gui, core
from .core import (
    module_reload,
    import_from_path,
    load_bundle,
    load_bundle_on_start,
    register_bundle_info,
    register_module,
    DevelExtrasSession,
    _write_bundles_md5,
    _replace_toolshed_methods,
    _restore_toolshed_methods,
)
from .gui import DevelExtrasTool


class _MyAPI(BundleAPI):

    # register_command called with CommandInfo instance instead of string
    api_version = 1

    commands = {
        "devel module": (core.ifp_desc, core.import_from_path),
        "devel module reload": (core.mr_desc, core.module_reload),
        "devel module register": (
            core.rmc_desc,
            core.register_module,
        ),
        "devel bundle": (core.lb_desc, core.load_bundle),
        "devel bundle info": (core.lbi_desc, core.load_bundle_info),
        "devel bundle register": (
            core.rbc_desc,
            core.register_bundle_info,
        ),
        "devel bundle onstart": (core.lbos_desc, core.load_bundle_on_start),
    }

    tools = {"DevelExtras Tool": gui.DevelExtrasTool}

    # Override method for starting tool
    @classmethod
    def start_tool(cls, session, bi, ti, **kw):
        if ti.name in cls.tools:
            tool = cls.tools[ti.name]
            return tool(session, ti.name)
        raise ValueError(f"trying to start unknown tool: {ti.name}")

    # Override method for registering commands
    @classmethod
    def register_command(cls, bi, ci, logger):
        if ci.name in cls.commands:
            desc, func = cls.commands[ci.name]
            if desc.synopsis is None:
                desc.synopsis = ci.synopsis
            register(ci.name, desc, func)

    @classmethod
    def register_all_commands(cls):
        for name in cls.commands:
            desc, func = cls.commands[name]
            register(name, desc, func)

    # Override method to support saving tools in sessions
    @classmethod
    def get_class(cls, class_name):
        for tool in cls.tools.values():
            if class_name == tool.__name__:
                return tool

    # Override method for initialization function called each time
    # ChimeraX starts.  Only invoked if the custom initialization
    # flag is set in bundle_info.xml.
    @staticmethod
    def initialize(session, bi):
        if session.debug:
            session.logger.info("Initialization")
        if not hasattr(session, "devel"):
            session.devel = DevelExtrasSession()
        _replace_toolshed_methods(session.toolshed)
        for bundle_path, on_start in session.devel.bundles_on_start.items():
            if on_start:
                load_bundle(session, bundle_path)

    # Override method for finalization function.
    # Only invoked if the custom initialization
    # flag is set in bundle_info.xml.
    @staticmethod
    def finish(session, bi):
        if session.debug:
            session.logger.info("Finish")
        if hasattr(session, "devel"):
            for _, bundle in session.devel.bundles.items():
                if bundle.custom_init:
                    bundle.finish(session)
            del session.devel
        _restore_toolshed_methods(session.toolshed)


bundle_api = _MyAPI()
