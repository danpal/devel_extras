#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 11 22:06:53 2021

.. codeauthor:: Daniel Esteban Palma Igor <daniel.palma.i@ug.uchile.cl>
"""

from chimerax.core.toolshed import BundleAPI
from chimerax.core.commands import register


class _MyAPI(BundleAPI):

    # register_command called with CommandInfo instance instead of string
    api_version = 1

    commands = {}

    tools = {}

    # Override method for starting tool
    @classmethod
    def start_tool(cls, session, bi, ti, **kw):
        if ti.name in cls.tools:
            tool = cls.tools[ti.name]
            return tool(session, ti.name)
        raise ValueError(f"trying to start unknown tool: {ti.name}")

    # Override method for registering commands
    @classmethod
    def register_command(cls, bi, ci, logger):
        if ci.name in cls.commands:
            desc, func = cls.commands[ci.name]
            if desc.synopsis is None:
                desc.synopsis = ci.synopsis
            register(ci.name, desc, func)

    @classmethod
    def register_all_commands(cls):
        for name in cls.commands:
            desc, func = cls.commands[name]
            register(name, desc, func)

    # Override method to support saving tools in sessions
    @classmethod
    def get_class(cls, class_name):
        for tool in cls.tools.values():
            if class_name == tool.__name__:
                return tool

    # Override method for initialization function called each time
    # ChimeraX starts.  Only invoked if the custom initialization
    # flag is set in bundle_info.xml.
    @staticmethod
    def initialize(session, bi):
        pass

    # Override method for finalization function.
    # Only invoked if the custom initialization
    # flag is set in bundle_info.xml.
    @staticmethod
    def finish(session, bi):
        pass

    @staticmethod
    def init_manager(session, bundle_info, name, **kwargs):
        """
        initialize test manager
        """
        if name == "test_manager":
            from .manager import TestManager

            session.test_manager = TestManager(session, name)
            return session.test_manager

    @staticmethod
    def run_provider(session, name, mgr, **kw):
        if mgr == session.test_manager:
            if name == "substitute_command":
                from .tests.substitute_command import SubstituteCmdTest

                return SubstituteCmdTest

            elif name == "normal_modes":
                from .tests.normal_modes import NormalModesToolTest

                return NormalModesToolTest


bundle_api = _MyAPI()
