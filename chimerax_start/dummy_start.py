#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 20 20:31:20 2020

@author: danpal
"""


from chimerax.dummy import load_bundle

# BUNDLE_PATH must be a directory and it must contain a bundle_info.xml and
# a src folder with an __init__.py that implements a bundle_api
bundle_path = "BUNDLE_PATH"
load_bundle(session, bundle_path)
