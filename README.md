# DevelExtras Bundle

DevelExtras is a [ChimeraX] Bundle that implements commands and a tool to use non
installed Bundles. Intended for use only in development of Bundles.

- Tool    :: DevelExtras Tool      :: Utilities :: A tool to open non installed
  Bundle Tools
- Command :: devel module          :: Utilities :: Imports Python module from file path
- Command :: devel module reload   :: Utilities :: Reloads the specified Python module and its submodules
- Command :: devel module register :: Utilities :: Registers a module's bundle
- Command :: devel bundle          :: Utilities :: Loads a Bundle from a folder path
- Command :: devel bundle info     :: Utilities :: Loads a BundleInfo object from a folder path
- Command :: devel bundle register :: Utilities :: Registers a loaded bundle
- Command :: devel bundle onstart  :: Utilities :: Controls whether a bundle is loaded at the ChimeraX start

The next functionalities are supported and have been tested on ChimeraX version 1.1:

- (Re)loading of Python modules
- Starting of Tools using the DevelExtras Tool
- Initialization of managers
- Adding of providers
- Registration of commands
- Registration of selectors

You can use the command `devel bundle` or the DevelExtras Tool to load your bundle from
the folder that contains the _bundle\_info.xml_, resulting in the creation of a
_BundleInfo_ object accesible from the _session.devel.bundles_ object, the loading
(or reloading) of the corresponding Python module, the initialization of managers,
the adding of providers, the registration of commands and the registration of
selectors specified in the XML, also all the tools specified in the XML can be started
through the DevelExtras Tool.

In most cases is NOT necessary to restart ChimeraX to reload a bundle or a
module but some specific cases may not work. Follow the next link to see some
caveats of module reloading:

https://docs.python.org/3/library/importlib.html#importlib.reload

Currently, every time that the information in the _bundle\_info.xml_ changes, is
necessary to build the bundle in order to collect the generated metadata.

If you want your Bundle loaded at the ChimeraX start, you should execute the
next command:

```devel bundle onload true BUNDLE_PATH```

BUNDLE_PATH must be a directory and it must follow the standard
[source code organization](https://www.cgl.ucsf.edu/chimerax/docs/devel/tutorials/introduction.html#source-code-organization).
It must contain a _bundle\_info.xml_ and a _src_ folder with an _\_\_init\_\_.py_ that implements a _bundle\_api_.

[ChimeraX]: https://www.cgl.ucsf.edu/chimerax/
