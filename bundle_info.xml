<!--
ChimeraX bundle names must start with "ChimeraX-"
to avoid clashes with package names in pypi.python.org.
When uploaded to the ChimeraX toolshed, the bundle
will be displayed without the ChimeraX- prefix.
-->

<BundleInfo name="ChimeraX-DevelExtras" version="0.4.2" package="chimerax.devel_extras"
	minSessionVersion="1" maxSessionVersion="1" customInit="true"
	supercedes="ChimeraX-Dummy">

  <!-- Additional information about bundle source -->
  <Author>Daniel Esteban Palma Igor</Author>
  <Email>daniel.palma.i@ug.uchile.cl</Email>
  <URL>https://gitlab.com/danpal/devel_extras/</URL>
  <License>LICENSE</License>

  <!-- Synopsis is a one-line description
       Description is a full multi-line description -->
  <Synopsis>Implements commands and a tool to use non installed Bundles.</Synopsis>
  <Description>DevelExtras is a ChimeraX Bundle that implements commands and a tool to use non
installed Bundles. Intended for use only in development of Bundles.

Tool    :: DevelExtras Tool      :: Utilities :: A tool to open non installed Bundle Tools
Command :: devel module          :: Utilities :: Imports Python module from file path
Command :: devel module reload   :: Utilities :: Reloads the specified Python module and its submodules
Command :: devel module register :: Utilities :: Registers a module's bundle
Command :: devel bundle          :: Utilities :: Loads a Bundle from a folder path
Command :: devel bundle info     :: Utilities :: Loads a BundleInfo object from a folder path
Command :: devel bundle register :: Utilities :: Registers a loaded bundle
Command :: devel bundle onstart  :: Utilities :: Controls whether a bundle is loaded at the ChimeraX start

The next functionalities are supported and have been tested on ChimeraX version 1.1:

- (Re)loading of Python modules
- Starting of Tools using the DevelExtras Tool
- Initialization of managers
- Adding of providers
- Registration of commands
- Registration of selectors

You can use the command devel bundle or the DevelExtras Tool to load your bundle from
the folder that contains the bundle_info.xml, resulting in the creation of a
BundleInfo object accesible from the session.devel.bundles object, the loading
(or reloading) of the corresponding Python module, the initialization of managers,
the adding of providers, the registration of commands and the registration of
selectors specified in the XML, also all the tools specified in the XML can be started
through the DevelExtras Tool.

In most cases is NOT necessary to restart ChimeraX to reload a bundle or a
module but some specific cases may not work. Follow the next link to see some
caveats of module reloading:

https://docs.python.org/3/library/importlib.html#importlib.reload

Currently, every time that the information in the bundle_info.xml changes, is
necessary to build the bundle in order to collect the generated metadata.

If you want your Bundle loaded at the ChimeraX start, you should execute the
next command:

devel bundle onload true BUNDLE_PATH

BUNDLE_PATH must be a directory and it must follow the standard
source code organization. It must contain a bundle_info.xml and a src folder
with an __init__.py that implements a bundle_api.
  </Description>

  <!-- Categories is a list where this bundle should appear -->
  <Categories>
    <Category name="Utilities"/>
  </Categories>

  <!-- Dependencies on other ChimeraX/Python packages -->
  <Dependencies>
    <Dependency name="ChimeraX-Core" version="~=1.4"/>
    <Dependency name="ChimeraX-UI" version="~=1.0"/>
  </Dependencies>

  <!-- Python and ChimeraX-specific classifiers
       From https://pypi.python.org/pypi?%3Aaction=list_classifiers
       Some Python classifiers are always inserted by the build process.
       These include the Environment and Operating System classifiers
       as well as:
         Framework :: ChimeraX
         Intended Audience :: Science/Research
         Programming Language :: Python :: 3
         Topic :: Scientific/Engineering :: Visualization
         Topic :: Scientific/Engineering :: Chemistry
         Topic :: Scientific/Engineering :: Bio-Informatics
       The "ChimeraX :: Bundle" classifier is also supplied automatically.  -->
  <Classifiers>
    <!-- Development Status should be compatible with bundle version number -->
    <PythonClassifier>Development Status :: 3 - Alpha</PythonClassifier>
    <PythonClassifier>License :: OSI Approved :: GNU General Public License v3 (GPLv3)</PythonClassifier>
    <ChimeraXClassifier>ChimeraX :: Tool :: DevelExtras Tool :: Utilities :: A tool to open non installed Bundle Tools</ChimeraXClassifier>
    <ChimeraXClassifier>ChimeraX :: Command :: devel module :: Utilities :: Imports Python module from file path</ChimeraXClassifier>
    <ChimeraXClassifier>ChimeraX :: Command :: devel module reload :: Utilities :: Reloads the specified Python module and its submodules</ChimeraXClassifier>
    <ChimeraXClassifier>ChimeraX :: Command :: devel module register :: Utilities :: Registers a module's bundle</ChimeraXClassifier>
    <ChimeraXClassifier>ChimeraX :: Command :: devel bundle :: Utilities :: Loads a Bundle from a folder path</ChimeraXClassifier>
    <ChimeraXClassifier>ChimeraX :: Command :: devel bundle info :: Utilities :: Loads a BundleInfo object from a folder path</ChimeraXClassifier>
    <ChimeraXClassifier>ChimeraX :: Command :: devel bundle register :: Utilities :: Registers a loaded bundle</ChimeraXClassifier>
    <ChimeraXClassifier>ChimeraX :: Command :: devel bundle onstart :: Utilities :: Controls whether a bundle is loaded at the ChimeraX start</ChimeraXClassifier>
  </Classifiers>

</BundleInfo>
