# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased](https://gitlab.com/danpal/devel_extras/-/compare/v0.4.0...master)

## [0.4.0](https://gitlab.com/danpal/devel_extras/-/compare/v0.3.0...v0.4.0) - 2021-01-14

### Added

- Support for managers
- Support for providers

### Changed

- Now the loaded bundles with custom initialization finish when DevelExtras finish
- Some commands descriptions

## [0.3.0](https://gitlab.com/danpal/devel_extras/-/compare/v0.2.0...v0.3.0) - 2020-12-17

### Added

- Command :: devel bundle onstart  :: Utilities :: Controls whether a bundle is
  loaded at the ChimeraX start
- New boolean option _loadOnStart_ to the command _devel bundle_.
- All the options of the command _devel bundle_ to the _DevelExtras Tool_

### Changed

- Bundle name from ChimeraX-Dummy to ChimeraX-DevelExtras
- Tool name from Dummy Tool to DevelExtras Tool
- Commands names from dummy to devel

## [0.2.0](https://gitlab.com/danpal/devel_extras/-/compare/v0.1.0...v0.2.0) - 2020-12-15 ###

### Added

- License GPLv3 for the distribution, the chimerax.dummy module and the
  chimerax.dummy.gui module.
- License MIT for the chimerax.dummy.core module.
- Command :: dummy module          :: Imports Python module from file path
- Command :: dummy module reload   :: Reloads the specified Python module and
  its  submodules
- Command :: dummy module register :: Registers commands of a dummy bundle's
  module
- Command :: dummy bundle          :: Loads a Bundle from the folder path
- Command :: dummy bundle info     :: Loads a Bundle Info from the folder path
- Command :: dummy bundle register :: Registers commands of a loaded dummy
  bundle

### Changed
- Tool :: Dummy Tool :: A tool to open non installed Bundle Tools
	- now can open any bundle from its directory.

## [0.1.0](https://gitlab.com/danpal/devel_extras/-/tags/v0.1.0) - 2020-11-06

### Added

- Tool :: Dummy Tool :: A Tool for opening other tools
